#pragma once

#include "jsonic/jsonic.h"

class TestDeserialization : public TestCase {
public:
    void test_simple_deserialization() {
        jsonic::String json = R"(
            { "number": 1, "list": [ 1, 2, 3], "string": "somestring", "none": null }
        )";

        jsonic::Node node;
        jsonic::loads(json, node);

        assert_true(1 == node["number"]);
        assert_true(1 == node["list"][0]);
        assert_true(2 == node["list"][1]);
        assert_true(3 == node["list"][2]);
        assert_true("somestring" == jsonic::String(node["string"]));
        assert_true(jsonic::None == node["none"]);
    }
};
